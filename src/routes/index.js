//import express from 'express';
const controllers = require('../controllers');
const express = require('express');
const route = express.Router();

route.get('/', controllers.listar);
route.post('/add', controllers.agregar);
route.get('/delete/:id', controllers.eliminar);
route.get('/update/:id', controllers.buscar);
route.post('/update/:id', controllers.actualizar);






module.exports = route;
