
const mysql = require('mysql2');
//require('dotenv').config();


const myConnection = mysql.createConnection({
    host: process.env.MYSQL,
    user: process.env.USER,
    password: process.env.MYSQL_PASS,
    database: process.env.DB
});
myConnection.connect((err, res) => {
    if (err) {
        console.log('error');
    } else {
        console.log('conecto a la base');
    }
});
module.exports = myConnection;