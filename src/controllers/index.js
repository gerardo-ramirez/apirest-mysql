myConnection = require('./../connections/');
controllers = {};

controllers.listar = (req, res) => {
    myConnection.query('SELECT * FROM customer', (err, data) => {
        if (err) {
            console.log('error en listar');

        } else {
            res.render('index.ejs', {
                data
            })
        };

    });

};
controllers.agregar = (req, res) => {
    const data = req.body;
    myConnection.query('INSERT INTO  customer set ?', [data], (err, dato) => {
        res.redirect('/');
    });

};
controllers.eliminar = (req, res) => {
    const { id } = req.params;
    myConnection.query('DELETE FROM customer WHERE id = ?', [id], (err, dato) => {
        res.redirect('/');
    });



};
controllers.buscar = (req, res) => {
    const { id } = req.params;
    myConnection.query('SELECT * FROM customer WHERE id = ?', [id], (err, conn) => {
        res.render('cambiar.ejs', {
            data: conn[0]
        })
    });
};
controllers.actualizar = (req, res) => {
    const { id } = req.params;
    const dato = req.body;
    myConnection.query('UPDATE customer set ? WHERE id = ?', [dato, id], (err, dat) => {
        res.redirect('/');
    });
}
module.exports = controllers;
