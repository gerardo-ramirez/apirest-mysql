require('dotenv').config();
const express = require("express");
const app = express();
const morgan = require('morgan');
const path = require('path');
const route = require('./routes/index');

//setting
app.set("port", process.env.PORT || 8080);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//middleware
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'));

//static file
app.use(express.static(path.join(__dirname)));

//routes
app.use(route);
//listen 

app.listen(app.get("port"), () => {
  console.log("conectado");
});
